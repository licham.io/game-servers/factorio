#!/bin/sh

echo "Starting nightly maintenance"
echo '/server-save' | arrcon -H ${RCON_SERVICE} -P ${RCON_PORT} -p ${RCON_PASSWORD} 

echo "Archiving nightly server backup"
mkdir -p ${PERSIST_PATH}/backups
tar -czvf "${PERSIST_PATH}/backups/$(date '+%Y-%m-%d_%H-%M-%S').tar.gz" ${PERSIST_PATH}/current

echo "Deleting backups older than ${KEEP_DAYS} days"
find ${PERSIST_PATH}/backups -mtime +14 -type f -delete

echo "Nightly mainenance complete"

# Always mark the job as succeeded, even if the server isn't up
true

