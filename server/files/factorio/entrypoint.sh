#!/bin/sh

# This script will run inside the container to start the server

exec /factorio/bin/x64/factorio \
  --port "${SERVER_PORT}" \
  --rcon-port "${RCON_PORT}" \
  --rcon-password "${RCON_PASSWORD}" \
  --server-settings /factorio/data/server-settings.json \
  --use-server-whitelist \
  --server-whitelist /factorio/data/server-whitelist.json \
  --server-adminlist /factorio/data/server-adminlist.json \
  --mod-directory /factorio/data/mods \
  --start-server-load-latest